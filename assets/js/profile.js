
let profileContainer = document.querySelector('#profileContainer')

function getItem(){

	 let token = localStorage.getItem('token')
  
	 console.log(token)
	
	if (!token || token === null) {
		alert("You must login first");
		window.location.replace("./login.html")
	} else {
		fetch('http://localhost:4000/api/users/details', {
			method: 'GET',
			header: {
				'Content-Type': 'application/json'
			},
			headers: {
            Authorization: `Bearer ${token}`
           }
		})
		.then(res => res.json())
		.then(data => {
     	 	console.log(data)

     	 let profileEnrollment = data.enrollments.map(classData => {
     	 		return(
     	 			`
					<tr>
				   		<td>${classData.courseId}</td> 
				   		<td>${classData.enrolledOn}</td> 
				   		<td>${classData.status}</td> 
					</tr> 
					`
     	 		)
     	 	}).join('')

     	 	document.getElementById("profileContainer").innerHTML = `
     	 	<div class="com-md-12">
				<section class="jumbotron jumbotron-fluid">
					<h2 class="text-center my-2"> Profile </h2>
					<hr class="w-50 mx-auto">
					<h5 class="text-center">Firstname: ${data.firstName}</h3>
					<h5 class="text-center">Lastname: ${data.lastName}</h3>
					<h5 class="text-center">Email: ${data.email}</h3>
					<h5 class="text-center">Mobile No: ${data.mobileNo}</h3>
					<br>
					<h3 class="text-center my-3">Class History</h3>
					<hr class="w-50 mx-auto">
					<table class="table">
						<thead>
							<tr>
								<th> CourseID </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
							<tbody>
				
							</tbody>
						</thead>
					</table>
				</section>
			</div>
		`

		})
	}


}


getItem()













