let courseContainer = document.querySelector('#courseContainer')
console.log('hello')


function getCourse(){

fetch('http://localhost:4000/api/courses', {
			method: 'GET',
			header: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => {
     	 	console.log(data)


     	let table = document.getElementById("courseContainer")

     	for (let i = 0; i < data.length; i++){
     			const row = `
     			<div class="col-md-12">
					<table class="table">
						<thead>
							<tr>
								<td>${data[i].createdOn}</td>
								<td>${data[i]._id}</td>
								<td>${data[i].name}</td>
								<td>${data[i].isActive}</td>
								<td></td>
								<td><button><i class="far fa-edit"></button></i></td>
								<td><button><i class="fas fa-trash"></i></button></i></td>
							</tr>
						</thead>
					</table>
				</div>
     	`

     	table.innerHTML += row
     		}

     	})
}

getCourse()

